let header = document.createElement('header')
let headerStyle = document.createElement('style')

headerStyle.innerHTML = `
header {
    width: 100%;
    padding: 15px 0;
    background-color: white;
}

.sayd_space{
    max-width: 1240px;
    margin: 0 auto;
    padding: 0 40px;
    display: flex;
    justify-content: space-between;
}

.sayd_header img {
    cursor: pointer;
}

.sayd_header{
    display: flex;
    align-items: center;
}

.vebmast,
.reclamo {
    cursor: pointer;
    margin-left: 40px;
    font-weight: 700;
    font-size: 13px;
    line-height: 16px;
    text-transform: uppercase;
    color: #565656;
}

.bottom{
    width:100%;
    padding: 10px 20px;
    background: #FFFFFF;
    box-shadow: 0px 10px 40px rgba(0, 0, 0, 0.05);
    display: flex;
    justify-content: space-between;
    margin-top: 12px;
    position: absolute;
    left: 0;
    bottom: 0;
}

.active{
    display: none;
}

.none{
    display: flex;
}

.exit{
    font-weight: 800;
    font-size: 13px;
    line-height: 16px;
    color: #55B570;
    margin-left: 50px;
    cursor: pointer;
}

@media only screen and (max-width: 550px){
    header {
        padding: 10px 20px;
        position: relative;
        height: 100px;
    }

    .active{
        display: flex;
    }

    .sayd_header img {
        width: 88px;
        height: 46px;
        object-fit: contain;
    }

    .none{
        display: none;
    }

    .sayd_header {
        display: flex;
        justify-content: center;
    }

    .sayd_space{
        justify-content: space-between;
        padding: 0px;
    }

    .vebmast,
    .reclamo {
    margin-left: 0px;
    }

    .activee_link{
        font-weight: 700;
        font-size: 12px;
        line-height: 15px;
        text-align: center;
        text-transform: uppercase;
        color: #565656;
        border-top: 2px solid #55B570;
    }

    .exit{
        margin-left: 0;
    }
}
`

header.innerHTML = `<div class="sayd_space">
    <div class="sayd_header"><img src="./img/logo 1.svg" class="none" alt="">
    <img src="./img/logo 1 (1).svg" class="active" alt="">
    </div>
    <div class="sayd_header none">
        <p class="vebmast none">вебмастерам</p>
        <p class="reclamo none">рекламодателям</p>
        <p class="exit">Выйти</p>
    </div>
</div >
<div class="bottom active">
    <p class="vebmast">вебмастерам</p>
    <p class="reclamo">рекламодателям</p>
</div>
`
document.querySelector('body').prepend(header, headerStyle)

let exit = document.querySelector('.exit')

exit.onclick = () => {
    window.location.href = './sign_up.html'
}