let form_register = document.querySelector('.form')
let api = 'https://peaceful-citadel-48682.herokuapp.com/users/'

form_register.onsubmit = () => {
    event.preventDefault()

    let new_coment = {
        id: Math.random(),

    }

    let fm = new FormData(form_register)

    let inut__email = document.querySelector('.inut__email')
    let small_inp = document.querySelectorAll('.small_inp')
    let label = document.querySelectorAll('.fle label')

    let regex = {
        email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        pasword: /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
    }

    if (regex.email.test(inut__email.value) == false) {
        inut__email.style.border = '2px solid red'
        label[0].style.display = 'block'
    } else {
        inut__email.style.border = '2px solid green'
        label[0].style.display = 'none'
    }

    if (regex.pasword.test(small_inp[0].value) == false) {
        small_inp[0].style.border = '2px solid red'
        label[1].style.display = 'block'
    } else {
        small_inp[0].style.border = '2px solid green'
        label[1].style.display = 'none'
    }

    if (inut__email.style.border == '2px solid green' && small_inp[0].style.border == '2px solid green') {
        fm.forEach((value, key) => {
            new_coment[key] = value
        })



        axios.post(api + 'create', new_coment)
            .then(res => {
                res.data = new_coment
                console.log(res);
            })
            .catch(err => {
                console.log(err)
            })


    }
}