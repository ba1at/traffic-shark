const ScrolTo = (element) => {
    window.scroll({
        left: 0,
        top: element.offsetTop,
        behavior: 'smooth',
    })
}

let img_svipe_header = document.querySelectorAll('.first_line img')
let to_header = document.querySelector('header')

for(let item of img_svipe_header){
    item.onclick = () => {
        ScrolTo(to_header)
    }

}