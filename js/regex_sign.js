let form_sign = document.querySelector('.second')

form_sign.onsubmit = () => {
    event.preventDefault()

    let person = {
        id: Math.random(),
    }

    let fm = new FormData(form_sign)

    let inp = document.querySelectorAll('.inp')
    let label = document.querySelectorAll('.fle label')

    let regex = {
        email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        pasword: /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
    }

    if (regex.email.test(inp[0].value) == false) {
        inp[0].style.border = '2px solid red'
        label[0].style.display = 'block'
    } else {
        inp[0].style.border = '2px solid green'
        label[0].style.display = 'none'
    }

    if (regex.pasword.test(inp[1].value) == false) {
        inp[1].style.border = '2px solid red'
        label[1].style.display = 'block'
    } else {
        inp[1].style.border = '2px solid green'
        label[1].style.display = 'none'
    }

    if(inp[0].style.border == '2px solid green' && inp[1].style.border == '2px solid green'){
        fm.forEach((value, key) => {
            person[key] = value
        })
        console.log(person);
    }
}